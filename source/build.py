#!/usr/bin/env python

"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys
import shutil

script_dir = os.path.dirname(os.path.abspath(__file__))

sys.path.append(script_dir + '/../manager/lib')
from docker_helper import Docker

IMAGE_TFS = "tfs:latest"
IMAGE_DATABASE = "tfs_db:latest"
IMAGE_WEB = "tfs_aac:latest"
IMAGE_WEB_INSTALLER = "tfs_aac_installer:latest"

print(" => [INFO] Build OTS server image...")
Docker.execute("build -t " + IMAGE_TFS + " .", working_dir=script_dir + "/modules/server/tfs")

print(" => [INFO] Build OTS database image...")
Docker.execute("build -t " + IMAGE_DATABASE + " .", working_dir=script_dir + "/modules/server/database")

print(" => [INFO] Build OTS web image...")
Docker.execute("build -t " + IMAGE_WEB + " .", working_dir=script_dir + "/modules/server/web")

print(" => [INFO] Build OTS web installer image...")
Docker.execute("build -t " + IMAGE_WEB_INSTALLER + " -f Dockerfile.installer .", working_dir=script_dir + "/modules/server/web")

# BUILD
if os.path.exists(script_dir + "/../build"):
    shutil.rmtree(script_dir + "/../build", ignore_errors=True)

print(" => Copy OTS managing scripts...")
shutil.copytree(script_dir + "/../manager", script_dir + "/../build")

print(" => Copy config...")
shutil.copytree(script_dir+"/config/server", script_dir+"/../build/config/server")

print(" => Save TFS image...")
Docker.execute("save -o " + script_dir + "/../build/modules/server/" + IMAGE_TFS.replace(":", "-") + ".tar " + IMAGE_TFS)

print(" => Save Database image...")
Docker.execute("save -o " + script_dir + "/../build/modules/server/" + IMAGE_DATABASE.replace(":", "-") + ".tar " + IMAGE_DATABASE)

print(" => Save Web image...")
Docker.execute("save -o " + script_dir + "/../build/modules/server/" + IMAGE_WEB.replace(":", "-") + ".tar " + IMAGE_WEB)

print(" => Save Web installer image...")
Docker.execute("save -o " + script_dir + "/../build/modules/server/" + IMAGE_WEB_INSTALLER.replace(":", "-") + ".tar " + IMAGE_WEB_INSTALLER)
