#!/usr/bin/env bash

printf " >> Waiting for DB...\n"
/srv/wait-for-it.sh tfsdb:3306 --timeout=300

printf "\n >> Start OTS...\n"
/bin/tfs