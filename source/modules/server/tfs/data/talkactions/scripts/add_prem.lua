local config = {
	maxDays = 365
}

function onSay(player, words, param)
	if configManager.getBoolean(configKeys.FREE_PREMIUM) then
		return true
	end

	if not player:getGroup():getAccess() then
		return true
	end

	if player:getAccountType() < ACCOUNT_TYPE_GOD then
		return false
	end

	local splittedParameters = param:split(",")
	if splittedParameters[1] == nil or splittedParameters[2] == nil then
		player:sendCancelMessage("Insufficient parameters.")
		return false
	end

	local target = Player(splittedParameters[1])
	if target == nil then
		player:sendCancelMessage("A player with that name is not online.")
		return false
	end

    -- Trim left
    splittedParameters[2] = splittedParameters[2]:gsub("^%s*(.-)$", "%1")
	local premDays = tonumber(splittedParameters[2])


	if target:getPremiumDays() < config.maxDays and premDays <= config.maxDays then
		target:addPremiumDays(premDays)
		target:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You have got " .. premDays .." days of premium account.")
		target:getPosition():sendMagicEffect(CONST_ME_POFF)

		player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "Player have got " .. premDays .." days of premium account.")
		player:getPosition():sendMagicEffect(CONST_ME_POFF)
	else
		player:sendCancelMessage("Player can not give more than " .. config.maxDays .. " days of premium account.")
		player:getPosition():sendMagicEffect(CONST_ME_POFF)
	end
	return false
end
