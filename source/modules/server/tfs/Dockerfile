FROM alpine:3.8 AS build
RUN apk add \
  binutils \
  boost-dev \
  build-base \
  clang \
  cmake \
  gcc \
  gmp-dev \
  lua-dev \
  luajit-dev \
  make \
  mariadb-connector-c-dev \
  pugixml-dev

COPY cmake /usr/src/forgottenserver/cmake/
COPY src /usr/src/forgottenserver/src/
COPY CMakeLists.txt /usr/src/forgottenserver/
WORKDIR /usr/src/forgottenserver/build
RUN cmake .. && make


FROM alpine:3.8
RUN apk add \
  boost-iostreams \
  boost-system \
  gmp \
  lua \
  luajit \
  mariadb-connector-c \
  pugixml \
  bash

COPY --from=build /usr/src/forgottenserver/build/tfs /bin/tfs
COPY start_tfs.sh /bin/start_tfs.sh
RUN chmod +x /bin/start_tfs.sh

COPY data /srv/data/
#COPY *.lua *.dist *.sql /srv/

COPY wait-for-it.sh /srv/wait-for-it.sh
RUN chmod +x /srv/wait-for-it.sh

EXPOSE 7171 7172

WORKDIR /srv

ENTRYPOINT ["/bin/start_tfs.sh"]
