#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir + '/../../lib')
from docker_helper import Docker
from ots_helper import wrap_with_color, fill_right_to_width
from ots_helper import COLOR_GREEN
from ots_helper import COLOR_RED

print(" => [" + wrap_with_color("WARNING", COLOR_RED) + "] Removing old secrets...")


def parse_result(result):
    if result is None:
        return "[" + wrap_with_color("ERROR", COLOR_RED) + "]"
    else:
        return "[" + wrap_with_color("REMOVED", COLOR_GREEN) + "]"


print(fill_right_to_width(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] ots.crt ", 32) + parse_result(
    Docker.secret().rm("ots.crt")))

print(fill_right_to_width(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] ots.key ", 32) + parse_result(
    Docker.secret().rm("ots.key")))
