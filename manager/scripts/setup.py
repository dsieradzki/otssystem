#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir + '/../lib')
from ots_helper import *

messages = get_lang_resources()


def install_secrets():
    ssl_env = os.getenv("OTS_SSL_DIR")
    if ssl_env is None or ssl_env.strip() == "":
        answer_ssl = confirm_user("To be safe, you have to set OTS_SSL_DIR environment variable with directory that contains 'ots.key' and 'ots.crt'"
                                  " files generated for your domain. You can install self signed certificate for testing but this is NOT RECOMMENDED. Do you want still use sample ssl certificates?")
        if answer_ssl is True:
            os.system(current_dir + "/../secrets/scripts/install.py")
            return
        else:
            return
    os.system(current_dir + "/../secrets/scripts/install.py")


print(messages["welcome_in_setup"] + "\n")

ssl_env = True if os.getenv("OTS_SSL_DIR") is not None else False
backup_env = True if os.getenv("OTS_BACKUP_DIR") is not None else False
server_env = True if os.getenv("FORGOTTEN_SERVER_IP") is not None else False

ssl_msg = wrap_with_color("AVAILABLE", COLOR_GREEN) if ssl_env else wrap_with_color("MISSING", COLOR_RED)
backup_msg = wrap_with_color("AVAILABLE", COLOR_GREEN) if backup_env else wrap_with_color("MISSING", COLOR_RED)
server_msg = wrap_with_color("AVAILABLE", COLOR_GREEN) if server_env else wrap_with_color("MISSING", COLOR_RED)

print("Your OTS needs few environment variable to work properly")
print(" => [" + wrap_with_color("RECOMMENDED", COLOR_RED) + "][" + ssl_msg + "] OTS_SSL_DIR - path to directory that contains 'ots.key' and 'ots.crt' ssl certificates for https")
print(" => [" + wrap_with_color("RECOMMENDED", COLOR_RED) + "][" + backup_msg + "] OTS_BACKUP_DIR - path to directory for ots backups - backup won't work without this variable")
print(" => [" + wrap_with_color("REQUIRED", COLOR_RED) + "][" + server_msg + "] FORGOTTEN_SERVER_IP - public IP of your machine - use the same ip as your domain")

pub_ip_env = os.getenv("FORGOTTEN_SERVER_IP")
if pub_ip_env is None or pub_ip_env.strip() == "":
    exit()

print("\n")

answer = confirm_user(
    "Before install OTS configuration, please configure OTS. Config files are in {OTS_PATH}/config directory. Have you done it?")
if answer is False:
    print(" => Please configure your OTS, and run setup again")
    exit()

# CONFIG
if Docker.config().is_exists("config-shop.php") or \
        Docker.config().is_exists("config.lua") or \
        Docker.config().is_exists("config.php") or \
        Docker.config().is_exists("ots-gate-config.conf"):
    answer = confirm_user("Configuration is already installed. Do you want to reinstall?")
    if answer is True:
        print("Reinstall config...")
        os.system(current_dir + "/../config/scripts/uninstall.py")
        os.system(current_dir + "/../config/scripts/install.py")

else:
    print("Install config...")
    os.system(current_dir + "/../config/scripts/install.py")

# SECRETS
if Docker.secret().is_exists("ots.crt") or \
        Docker.secret().is_exists("ots.key"):
    answer = confirm_user("Secrets are already installed. Do you want to reinstall?")
    if answer is True:
        os.system(current_dir + "/../secrets/scripts/uninstall.py")
        install_secrets()
else:
    install_secrets()

os.system(current_dir + "/../modules/server/scripts/install.py")
os.system(current_dir + "/../modules/server/scripts/start.py")

print("\n\nYour setup is almost finished, last step is to visit {SERVER_IP}:8011 and configure MyAAC, remember to block 8011 port when you finish (in future this step of setup will be changed)")
print("\n\n During installation of MyAAC in Basic configuration step, please fill fields as below:")
print("\nServer path: /srv/ <- THIS IS VERY IMPORTANT")
print("Client version: 10.98\n")
