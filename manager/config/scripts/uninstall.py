#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../lib')
from docker_helper import Docker
from ots_helper import wrap_with_color
from ots_helper import COLOR_RED
from ots_helper import COLOR_GREEN
from ots_helper import fill_right_to_width

print("")

print(" => ["+wrap_with_color("WARNING", COLOR_RED)+"] Removing old configs...")


def parse_result(result):
    if result is None:
        return "[" + wrap_with_color("ERROR", COLOR_RED) + "]"
    else:
        return "[" + wrap_with_color("REMOVED", COLOR_GREEN) + "]"


print(fill_right_to_width(" => [INFO] ots-gate-config.conf ", 32) + parse_result(
    Docker.config().rm("ots-gate-config.conf")))

print(fill_right_to_width(" => [INFO] config.lua ", 32) + parse_result(
    Docker.config().rm("config.lua")))

print(fill_right_to_width(" => [INFO] config.php ", 32) + parse_result(
    Docker.config().rm("config.php")))

print(fill_right_to_width(" => [INFO] config-shop.php ", 32) + parse_result(
    Docker.config().rm("config-shop.php")))

print ("")
