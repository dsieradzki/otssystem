#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../lib')

from ots_helper import get_service_status_text
from ots_helper import fill_right_to_width
from data import OTS_CONTAINERS

for k, v in OTS_CONTAINERS.iteritems():
    print(fill_right_to_width(k, 9) + "[" + get_service_status_text(v) + "]")

print("")
