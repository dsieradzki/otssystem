#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import sys
import os
import time

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir + '/../../../lib')
from docker_helper import Docker
from ots_helper import print_error
from data import OTS_CONTAINERS

print(" => [INFO] Removing OTS stack...")
Docker.execute("stack rm ots")

print("")

attempts = 0
while True:
    if attempts > 10:
        print_error("=> [ERROR] Cannot start containers")
        break
    attempts += 1

    stopped_containers = [v for k, v in OTS_CONTAINERS.iteritems() if not Docker.container().is_running(v)]

    if len(stopped_containers) is len(OTS_CONTAINERS):
        break
    time.sleep(3)
    print("=> [INFO] Waiting for stopping containers ...")

print("")

os.system(current_dir + "/status.py")
