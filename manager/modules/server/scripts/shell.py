#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../lib')
from data import OTS_CONTAINERS

from ots_helper import print_error
from docker_helper import Docker

if len(sys.argv) <= 1 or len(sys.argv[1].strip()) == 0:
    print(" => Available services:")

    containers = {}
    index = 1
    for key, value in OTS_CONTAINERS.iteritems():
        containers[index] = value
        print ("\t" + str(index) + ". " + key + " => " + value)
        index += 1

    user_choice = raw_input("\nPlease select service[1-" + str(len(containers)) + "]: ")

    if not user_choice.isdigit():
        container_name = None
    else:
        container_name = containers[int(user_choice)]

else:
    container_name = sys.argv[1]

container_id = Docker.container().id(container_name)

if container_id is None:
    print_error(" => Cannot find service with given name\n")
    exit()

Docker.execute("exec -it " + container_id + " sh")
