#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir + '/../../../lib')
from docker_helper import Docker
from ots_helper import *

os.system(current_dir + "/stop.py")

print(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] Clean stopped containers...")

Docker.execute("container prune -f")

print(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] Removing OTS images...")

print("    " + Docker.execute("rmi -f tfs:latest", stdout=False))

print("    " + Docker.execute("rmi -f tfs_db:latest", stdout=False))

print("    " + Docker.execute("rmi -f tfs_aac:latest", stdout=False))

print("    " + Docker.execute("rmi -f tfs_aac_installer:latest", stdout=False))

if len(sys.argv) == 2 and sys.argv[1] == "--with-data":
    if confirm_user("Are you sure do you want delete volumes?"):
        print(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] Removing OTS volumes...")
        print("    " + Docker.execute("volume rm ots_tfs_data -f", stdout=False))
        print("    " + Docker.execute("volume rm ots_tfs_db_data -f", stdout=False))
        print("    " + Docker.execute("volume rm ots_tfs_aac_config -f", stdout=False))
else:
    print("\n => [" + wrap_with_color("SKIP", COLOR_GREEN) + "] Removing OTS volumes... to uninstall OTS with data please add '--with-data' argument")


print("\n Info: Configuration and Secrets are not deleted, to do this run appropriate commands")
