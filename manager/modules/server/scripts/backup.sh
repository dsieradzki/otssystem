#!/usr/bin/env bash
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

if [[ -z "${OTS_BACKUP_DIR}" ]]; then
  echo " => [ERROR] You have to set OTS_BACKUP_DIR environment variable with backup directory.";
  exit -1
fi

BACKUP_DIR=$OTS_BACKUP_DIR
DATE=`date +%Y%m%d%H%M%S`
BACKUP_FILE_NAME="backup_ots_db.sql"
VOLUME_TO_BACKUP="ots_tfs_aac_config"

if [ $# -lt 2 ]
   then
   printf " => DB User: "
   read DB_USER

   printf " => DB Password: "
   read DB_PASSWORD
else
   DB_USER=$1
   DB_PASSWORD=$2
fi

printf " => Comment: "
read BACKUP_COMMENT

if [ ! -d "$BACKUP_DIR" ]; then
	echo " => [INFO] Backup directory doesn't exists. Create..."
	mkdir $BACKUP_DIR; 
fi

echo " => [INFO] Create directory for new backup..."
mkdir "$BACKUP_DIR/$DATE"


echo " => [INFO] Backup OTS Database..."
docker run -it  -v $BACKUP_DIR:/backup --network=host alpine:3.8 sh -c "apk add mysql-client 1>/dev/null && apk add pv 1>/dev/null && mysqldump -h 127.0.0.1 -u""$DB_USER"" -p""$DB_PASSWORD"" tfs | pv > /backup/""$DATE""/""$BACKUP_FILE_NAME"
	


echo " => [INFO] Backup $VOLUME_TO_BACKUP volume..."
docker run -it --rm -v $VOLUME_TO_BACKUP:/volume -v $BACKUP_DIR:/backup alpine:3.8 \
    tar -cjf /backup/$DATE/$VOLUME_TO_BACKUP.tar.bz2 -C /volume ./


echo " => [INFO] Save backup comment..."
echo $BACKUP_COMMENT > $BACKUP_DIR/$DATE/comment.txt