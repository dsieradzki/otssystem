#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""


import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../lib')
from docker_helper import Docker
from ots_helper import wrap_with_color
from ots_helper import COLOR_GREEN

script_dir = os.path.dirname(os.path.abspath(__file__))

print(" => [" + wrap_with_color("INFO", COLOR_GREEN) + "] Install OTS images...")

print(" Load TFS image...")
Docker.execute("load --input " + script_dir + "/../tfs-latest.tar")

print(" Load Database image...")
Docker.execute("load --input " + script_dir + "/../tfs_db-latest.tar")

print(" Load Web image...")
Docker.execute("load --input " + script_dir + "/../tfs_aac-latest.tar")

print(" Load Web installer image...")
Docker.execute("load --input " + script_dir + "/../tfs_aac_installer-latest.tar")
