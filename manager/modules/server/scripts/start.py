#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

import os
import sys
import time

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir + '/../../../lib')

from docker_helper import Docker
from ots_helper import print_error
from data import OTS_CONTAINERS

if os.environ.get("FORGOTTEN_SERVER_IP") is None:
    print_error(" => [ERROR] You have to set FORGOTTEN_SERVER_IP environment variable with public IP.\n")
    exit()

print(" => [INFO] Starting OTS stack...")
Docker.execute("stack deploy -c deployment.yml ots",
               working_dir=os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

print("")

attempts = 0
while True:
    if attempts > 10:
        print_error("=> [ERROR] Cannot start containers")
        break
    attempts += 1

    running_containers = [v for k, v in OTS_CONTAINERS.iteritems() if Docker.container().is_running(v)]

    if len(running_containers) is len(OTS_CONTAINERS):
        break
    time.sleep(3)
    print("=> [INFO] Waiting for starting containers ...")

print("")

os.system(current_dir + "/status.py")
