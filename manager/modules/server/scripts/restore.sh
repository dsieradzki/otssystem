#!/usr/bin/env bash
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

if [[ -z "${OTS_BACKUP_DIR}" ]]; then
  echo " => [ERROR] You have to set OTS_BACKUP_DIR environment variable with backup directory.";
  exit -1
fi


BACKUP_DIR=$OTS_BACKUP_DIR
BACKUP_FILE_NAME="backup_ots_db.sql"
VOLUME_TO_BACKUP="ots_tfs_aac_config"


if [ $# -lt 2 ]
   then
   printf " => DB User: "
   read DB_USER

   printf " => DB Password: "
   read DB_PASSWORD
   echo
else
   DB_USER=$1
   DB_PASSWORD=$2
fi


BACKUP_DIR_ENTRIES=($(ls $BACKUP_DIR | sort -r))

INDEX=0
for BACKUP in ${BACKUP_DIR_ENTRIES[@]}
do
    COMMENT_CONTENT=""
    if [ -f $BACKUP_DIR/$BACKUP/comment.txt ]; then
        COMMENT_CONTENT=$(<$BACKUP_DIR/$BACKUP/comment.txt)
    fi
    echo "    "$INDEX" => "$BACKUP" | "$COMMENT_CONTENT
    INDEX=$((INDEX+1))
done

echo ""

printf " => [INFO] Please specify backup to restore [0 => latest backup]:"
read BACKUP_NUMBER

if [ "$BACKUP_NUMBER" = "" ]
then
    echo " => [ERROR] You have to specify backup"
    exit -1
fi

printf " => Are you sure? You will delete current OTS STATE!!! [yes]:"
read RESPORE_CONFIRM

if [[ "$RESPORE_CONFIRM" = "yes" ]]
then
    echo " => [INFO] Restore backup ${BACKUP_DIR_ENTRIES[$BACKUP_NUMBER]} ..."
    
    echo " => [INFO] Restore database..."
    docker run -it  -v $BACKUP_DIR:/backup --network=host alpine:3.8 sh -c "apk add mysql-client 1>/dev/null && apk add pv 1>/dev/null && pv /backup/""${BACKUP_DIR_ENTRIES[$BACKUP_NUMBER]}""/""$BACKUP_FILE_NAME"" | mysql -h 127.0.0.1 -u""$DB_USER"" -p""$DB_PASSWORD"" tfs"

    echo " => [INFO] Restore $VOLUME_TO_BACKUP volume..."
    docker run -it --rm -v $VOLUME_TO_BACKUP:/volume -v $BACKUP_DIR:/backup alpine:3.8 \
    tar -xvjf /backup/${BACKUP_DIR_ENTRIES[$BACKUP_NUMBER]}/$VOLUME_TO_BACKUP.tar.bz2 -C /volume ./

    else
    echo " => [INFO] Backup is NOT restored."
fi

echo " => [INFO] Success"