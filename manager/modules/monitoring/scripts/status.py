#!/usr/bin/env python
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""


import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../lib')
from ots_helper import get_service_status_text

print("Swarmprom Caddy            [" + get_service_status_text("swarmprom_caddy") + "]")
print("Swarmprom Grafana          [" + get_service_status_text("swarmprom_grafana") + "]")
print("Swarmprom Prometheus       [" + get_service_status_text("swarmprom_prometheus") + "]")
print("Swarmprom Alertmanager     [" + get_service_status_text("swarmprom_alertmanager") + "]")
print("Swarmprom Unsee            [" + get_service_status_text("swarmprom_unsee") + "]")
print("Swarmprom Cadvisor         [" + get_service_status_text("swarmprom_cadvisor") + "]")
print("Swarmprom Dockerd-exporter [" + get_service_status_text("swarmprom_dockerd-exporter") + "]")
print("Swarmprom Node-exporter    [" + get_service_status_text("swarmprom_node-exporter") + "]")

print("")
