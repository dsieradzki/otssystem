#!/usr/bin/env bash
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""
current_dir="$(dirname "$0")"
"$current_dir/stop.sh"


echo " => [INFO] Clean stopped containers..."
    docker container prune -f
echo ""


printf "=====================================\n"
printf "=== Removing Portainer.io volumes ===\n"
printf "=====================================\n"
      docker volume rm portainer_portainer_data -f
printf "=====================================\n"