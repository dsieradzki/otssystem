"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

from docker_helper import Docker
import subprocess
import importlib
import locale
import os
import sys

COLOR_RED = '\33[31m'
COLOR_YELLOW = '\33[33m'
COLOR_GREEN = '\33[92m'
COLOR_BLUE = '\33[94m'
COLOR_END = '\33[0m'


def get_service_status_text(service_name):
    return {
        "STOPPED": wrap_with_color("STOPPED", COLOR_RED),
        "ERROR": wrap_with_color("ERROR", COLOR_RED),
        "STARTING": wrap_with_color("STARTING", COLOR_YELLOW),
        "RUNNING": wrap_with_color("RUNNING", COLOR_GREEN)
    }[Docker.service().status(service_name)]


def print_error(value):
    print(COLOR_RED + value + COLOR_END)


def wrap_with_color(value, color):
    return color + value + COLOR_END


def fill_right_to_width(name, count):
    expression = "{0: <" + str(count) + "}"
    return expression.format(str(name))


def check_docker():
    try:
        result = subprocess.check_output(["which", "docker"], stderr=(open(os.devnull, "w")))
    except subprocess.CalledProcessError:
        result = None

    if result is not None and result.strip() is not "":
        docker_version = Docker.execute("info", stdout=False)
        if docker_version is None or docker_version.startswith("Cannot connect to the Docker daemon"):
            print(fill_right_to_width("Docker", 21) + " [" + wrap_with_color("NOT RUNNING", COLOR_RED) + "]")
            exit()
        else:
            print(fill_right_to_width("Docker", 21) + " [" + wrap_with_color("RUNNING", COLOR_GREEN) + "]")
    else:
        print(fill_right_to_width("Docker", 21) + " [" + wrap_with_color("NOT FOUND", COLOR_RED) + "]")
        print("Please install Docker. https://docs.docker.com/install/#supported-platforms\n")
        exit()

    is_swarm = Docker.is_swarm_mode()
    if is_swarm:
        print(fill_right_to_width("Swarm Mode", 21) + " [" + wrap_with_color("ACTIVE", COLOR_GREEN) + "]")
    else:
        print(fill_right_to_width("Swarm Mode", 21) + " [" + wrap_with_color("INACTIVE", COLOR_RED) + "]")
        answer = raw_input("\nYou you want enable Swarm Mode?[Y/*] ")
        if answer == "Y" or answer == "y":
            Docker.execute("swarm init")
            print("\n Thank you, now you can run OTS")
            exit()
        else:
            print_error("OTS will not work when Swarm Mode is disabled")
            exit()

    is_experimental = Docker.is_experimental_enabled()
    if not is_experimental:
        print("Experimental features [" + wrap_with_color("DISABLED",
                                                          COLOR_RED) + "] - OTS will work properly but experimental features are required for run monitoring module")
    print("")


def get_lang_resources():
    try:
        default_language = locale.getdefaultlocale()[0]
        if os.path.exists("../lib/lang/{file}.py".format(file=default_language)):
            return importlib.import_module("lang." + default_language).lang
        else:
            raise ImportError
    except:
        return importlib.import_module("lang.en_US").lang


def read_input(prompt=None):
    if sys.hexversion >= 0x3000000:
        return input(prompt)
    else:
        return raw_input(prompt)


def ask_user(question):
    return read_input("[" + wrap_with_color("?", COLOR_GREEN) + "] " + question + ": ")


def confirm_user(question):
    answer = read_input("[" + wrap_with_color("?", COLOR_GREEN) + "] " + question + ": (y/N) ")
    if answer is not None and (answer == "Y" or answer == "y"):
        return True
    else:
        return False
