# coding=utf-8
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""

lang = {
    "usage_ots_tool": "Usage: ots <command> [subcommand] [parameters]",
    "list_of_commands": "List of Commands",
    "usage_ots_tool_module": "Usage: ots {module} <subcommand> [parameters]",
    "list_of_subcommands": "List of Subcommands",
    "error_command_not_found": "Error: command not found",
    "error_subcommand_not_found": "Error: Subcommand not found",

    "welcome_in_setup": "Welcome in OTS setup, after end of setup, you will have working OTS."
}
