
"""
OTS Tool - a free and open-source tool for managing Open Tibia Server
Copyright (C) 2018  Damian Sieradzki <damian.sieradzki@hotmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""


import subprocess
import os


class Docker(object):

    @staticmethod
    def execute(command, working_dir=None, stdout=True):
        if working_dir is not None:
            os.chdir(working_dir)
        if stdout is True:
            os.system("docker " + command)
        else:
            try:
                output = subprocess.check_output("docker " + command, shell=True, stderr=subprocess.STDOUT)
                if output is not None:
                    output = output.strip()
                return output
            except subprocess.CalledProcessError:
                return None

    @staticmethod
    def config():
        return DockerConfig

    @staticmethod
    def container():
        return DockerContainer

    @staticmethod
    def service():
        return DockerService

    @staticmethod
    def secret():
        return DockerSecrets

    @staticmethod
    def version():
        result = Docker.execute("version --format '{{.Server.Version}}'", stdout=False)
        if result is None or result.strip() == "":
            return None
        else:
            return result.strip()

    @staticmethod
    def is_swarm_mode():
        result = Docker.execute("info --format '{{.Swarm.LocalNodeState}}'", stdout=False)
        if result is None or result == "inactive":
            return False
        else:
            return True

    @staticmethod
    def is_experimental_enabled():
        result = Docker.execute("version --format '{{.Server.Experimental}}'", stdout=False)
        if result is not None and result == "true":
            return True
        else:
            return False


class DockerConfig:
    def __init__(self):
        pass

    @staticmethod
    def rm(name):
        return Docker.execute(" config rm " + name, stdout=False)

    @staticmethod
    def create(name, file_name):
        return Docker.execute(" config create " + name + " " + file_name, stdout=False)

    @staticmethod
    def is_exists(name):
        result = Docker.execute(" config ls --format '{{.Name}}' | grep '" + name + "' -w",
                                stdout=False)
        if result is not None and result.strip() != "":
            return True
        else:
            return False


class DockerSecrets:
    def __init__(self):
        pass

    @staticmethod
    def rm(name):
        return Docker.execute(" secret rm " + name, stdout=False)

    @staticmethod
    def create(name, file_name):
        return Docker.execute(" secret create " + name + " " + file_name, stdout=False)

    @staticmethod
    def is_exists(name):
        result = Docker.execute(" secret ls --format '{{.Name}}' | grep '" + name + "' -w",
                                stdout=False)
        if result is not None and result.strip() != "":
            return True
        else:
            return False


class DockerContainer:
    def __init__(self):
        pass

    @staticmethod
    def is_running(service_container_name):
        try:
            docker_status_output = subprocess.check_output(
                "docker ps -f name=" + service_container_name + " --format '{{.Names}}' | grep -w " + service_container_name,
                stderr=subprocess.STDOUT,
                shell=True)

        except subprocess.CalledProcessError:
            return False

        if len(docker_status_output) == 0:
            return False

        return True

    @staticmethod
    def id(name):
        if name is None:
            return None
        try:
            docker_status_output = subprocess.check_output(
                "docker ps -f name=" + name + " --format '{{.Names}} {{.ID}}' | grep -w " + name + " | awk '{print $2}'",
                stderr=subprocess.STDOUT,
                shell=True)

        except subprocess.CalledProcessError:
            return None

        if len(docker_status_output.strip()) == 0:
            return None

        return docker_status_output.strip()


class DockerService:
    DOCKER_ERROR = "Cannot connect to the Docker daemon"

    def __init__(self):
        pass

    @staticmethod
    def status(service_name):
        docker_status_output = subprocess.check_output(
            "docker service ls -f name=" + service_name + " | grep -w " + service_name + " | awk '{print $4}'",
            stderr=subprocess.STDOUT,
            shell=True)

        if len(docker_status_output) == 0 or DockerService.DOCKER_ERROR in docker_status_output:
            return "STOPPED"

        docker_status = docker_status_output.split("/")

        started_services = int(docker_status[0])
        expected_services = int(docker_status[1])

        if started_services == 0 and expected_services == 0:
            return "ERROR"

        if started_services < expected_services:
            return "STARTING"

        return "RUNNING"

    @staticmethod
    def id(name):
        if name is None:
            return None
        try:
            docker_status_output = subprocess.check_output(
                "docker service ls -f name=" + name + " --format '{{.ID}} {{.Name}}' | grep -w " + name + " | awk '{print $2}'",
                stderr=subprocess.STDOUT,
                shell=True)

        except subprocess.CalledProcessError:
            return None

        if len(docker_status_output.strip()) == 0:
            return None

        return docker_status_output.strip()

    @staticmethod
    def logs(name):
        if name is None:
            return None
        try:
            service_id = Docker.service().id(name)

            docker_status_output = subprocess.check_output(
                "docker service logs " + service_id,
                stderr=subprocess.STDOUT,
                shell=True)

        except subprocess.CalledProcessError:
            return None

        if len(docker_status_output.strip()) == 0:
            return None

        return docker_status_output.strip()
